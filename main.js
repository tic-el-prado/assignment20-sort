import { sort } from "./sort.js";

function randomList(n, min, max) {
	let list = [];
	for (let i = 0; i < n; i++) {
		let x = Math.floor(Math.random() * (max - min) + min);
		list.push(x);
	}
	return list;
}

let genButton = document.getElementById("gen");
let input = document.getElementById("list");
let inputList = randomList(10, 0, 50);
let output = document.getElementById("output");
genButton.addEventListener("click", () => {
	input.value = JSON.stringify(inputList, null, " ");
});

let sortButton = document.getElementById("sort");
sortButton.addEventListener("click", () => {
	let outputList = sort(inputList);
	output.value = JSON.stringify(outputList, null, " ");
});