In this assignment, you will implement a sorting algorithm.

Quick links:

-   [How to start an assignment](https://youtu.be/r99anCIBMeU)
-   [Run the program (after clicking on `Go Live` in VSCode)](http://localhost:9000)
-   [How to submit an assignment](https://youtu.be/rkIN7srfthI)


# Statement

Implement the *select sort* algorithm, writing a function called `sort` that receives a list of numbers as argument and returns the same list but with the numbers in order, from smallest to biggest.

![Watch the expected result](./assignment20-sort.mp4)

You are given a web page that generates a random list of numbers, and that will call the function you define and show the results when the user clicks on a button.

You only need to write the `sort` function definition.

Write your code inside the file named `sort.js`.


# How to start the assignment

Watch and follow the steps in the next tutorial (but choose the right assignment number, which might be different from the one you see in the video):

[How to start an assignment](https://youtu.be/r99anCIBMeU)

To sum up, the steps are:

1.  Fork the project in your GitLab account.
2.  Clone your copy of the project in VSCode.


# Submission

Follow these steps to submit your assignment:

[How to submit an assignment](https://youtu.be/rkIN7srfthI)

To sum up, the steps are:

1.  In VSCode, click on the `Source control` button in the left panel.
2.  Write a message in the box (anything you want to identify the version you are uploading).
3.  Click on the `Commit` button.
4.  Click on the `Sync Changes` button.
5.  Open your `Documents` folder in Windows File Explorer.
6.  Right-click on the assignment&rsquo;s folder, select `Send to`, and then select `Compressed (zipped) folder`.
7.  In Schoology, click on the `Submit assignment` button, and, in the `Upload` tab, press the button with a document icon, to attach the zip file you have just created in the previous step.
8.  Go back to your `Documents` folder, and delete the compressed file.

After submission of the assignment, you can watch your program running online at this address (replace *user* with your GitLab username)

<https://user.pages.gototic.com/assignment19-algorithms1>
